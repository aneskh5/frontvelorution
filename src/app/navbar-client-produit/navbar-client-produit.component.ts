import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-navbar-client-produit',
  templateUrl: './navbar-client-produit.component.html',
  styleUrls: ['./navbar-client-produit.component.css']
})
export class NavbarClientProduitComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  onSubmit() {  
    this.router.navigateByUrl('/Navbar-admin');
}
}
