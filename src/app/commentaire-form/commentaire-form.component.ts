
import { Component, OnInit ,NgZone,Input} from '@angular/core';
import {FormBuilder, FormGroup , ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Router} from "@angular/router";
import {CrudPublicationService} from "../service/crud-publication.service";
import {CrudCommentaireService} from "../service/crud-commentaire.service";
import { analyzeAndValidateNgModules } from '@angular/compiler';
@Component({
  selector: 'app-commentaire-form',
  templateUrl: './commentaire-form.component.html',
  styleUrls: ['./commentaire-form.component.css']
})
export class CommentaireFormComponent implements OnInit {
  
  @Input() message : any;
  commentaireForm: FormGroup;
  selectedType! : any;
  showForm:boolean;
  constructor(public formBuilder: FormBuilder,public ReactiveFormsModule:ReactiveFormsModule,
              private router: Router,
              private ngZone: NgZone,
              private crudService: CrudPublicationService,
              private crudServicecom: CrudCommentaireService,
              private ReactiveForms: ReactiveFormsModule,) {

    this.commentaireForm = this.formBuilder.group({
      
      description: [''],
     
      
      
    })

  }

  ngOnInit(): void {

  }
  show(){
    this.showForm=true;
  }

  onSubmit(): any {
    
    console.log(this.message)
    this.crudServicecom.AddCommentaire(this.commentaireForm.value,this.message)
    
  
      .subscribe(() => {
        console.log('Data added successfully!')
        this.ngZone.run(() => this.router.navigateByUrl(`listepublication/OneShow/${this.message}`))
        console.log(this.commentaireForm.value)
      }, (err) => {
        console.log(err);
      });
      this.showForm=false;
  }
  
}