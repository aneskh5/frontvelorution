import { Component, OnInit ,NgZone, Input} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CrudEvenementService} from "../service/crud-evenement.service";
import {UserService} from "../service/users/user.service";
import {User} from "../models/User";
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {


  eventForm: FormGroup;
  selectedType! : any;
  user:any;   
  roles:any;   
  @Input() solo:any; 

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              private ngZone: NgZone,
              private ar:ActivatedRoute,
              private UserService: UserService) {

  }

  ngOnInit(): void {
    let id:any;
    this.user = new User();
    this.roles = ["admin",'client','public'] ;
    if(this.ar.snapshot.params['id'])
      this.getUserById();
    
    console.log('this.ar.snapshot.params ===>  ',this.ar.snapshot.params['id'])
  }

  getUserById(){

    this.UserService.getUserById(this.ar.snapshot.params['id']).subscribe(
      (res)=>{
        this.user = res.data;
      },
      (error)=>{
        console.log(error);
      }
      );
  }

  updateUser(f:any){
    this.UserService.updateUser(f,this.ar.snapshot.params['id'] )
      .subscribe(() => {
        console.log('Data added successfully!',f)
        // this.ngZone.run(() => this.router.navigateByUrl('/users'))
        this.router.navigate(['users']);

      }, (err) => {
        console.log(err);
      });
  }

  addUser(f:any){
    console.log('form ==> ',f)
    this.UserService.addUser(f).subscribe(
      (data)=>{
        // this.router.navigate(['users']);
        window.location.reload();

      },
      (error)=>{
        console.log(error);
      }
    );
  }


  onSubmit(f:any): any {
    if(this.ar.snapshot.params['id']){
        this.updateUser(f);
      }else{
      this.addUser(f);
    }
  }
}