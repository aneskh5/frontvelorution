import { Component, OnInit ,NgZone} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {CrudProduitService} from "../service/crud-produit.service";
@Component({
  selector: 'app-produit-form',
  templateUrl: './produit-form.component.html',
  styleUrls: ['./produit-form.component.css']
})
export class ProduitFormComponent implements OnInit {

  produitForm: FormGroup;
  selectedType! : any;

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              private ngZone: NgZone,
              private crudService: CrudProduitService) {

    this.produitForm = this.formBuilder.group({
      titre: [''],
      image: [''],
      price: [''],
      stock: [''],
      
    })

  }

  ngOnInit(): void {

  }

  onSubmit(): any {
    this.crudService.Addproduit(this.produitForm.value, this.selectedType)
      .subscribe(() => {
        console.log('Data added successfully!')
        this.ngZone.run(() => this.router.navigateByUrl('/produits'))
      }, (err) => {
        console.log(err);
      });
  }
}