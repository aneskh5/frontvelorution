import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontEventAddComponent } from './front-event-add.component';

describe('FrontEventAddComponent', () => {
  let component: FrontEventAddComponent;
  let fixture: ComponentFixture<FrontEventAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontEventAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontEventAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
