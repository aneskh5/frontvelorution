import {Component, NgZone, OnInit} from '@angular/core';
import {CrudInscriptionService} from "../service/crud-inscription.service";
import {Router} from "@angular/router";
import {CrudFormationService} from "../service/crud-formation.service";


@Component({
  selector: 'app-formation',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {


  inscriptions:any=[];
  showForm:boolean;

  constructor(private crudInscriptionService: CrudInscriptionService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.showForm=false;
      this.crudInscriptionService.GetInscription().subscribe((res) =>{
        this.inscriptions= res.data
        console.log('res.data ',res.data)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });

    }

    show(){
      this.showForm=true;
    }

    delete(id:any) {
      {
        this.crudInscriptionService.deleteInscription(id).subscribe(
          (data) =>{
            console.log('delete user ==> ',data)
            window.location.reload();
          },
          (err) =>{
            console.log('err  ==> ',err)
          }
        )
      }
    }
    reloadCurrentPage() {
      window.location.reload();
     }
      }
    



