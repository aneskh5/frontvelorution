import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontProduitAddComponent } from './front-produit-add.component';

describe('FrontProduitAddComponent', () => {
  let component: FrontProduitAddComponent;
  let fixture: ComponentFixture<FrontProduitAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontProduitAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontProduitAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
