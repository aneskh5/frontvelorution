import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { CrudInscriptionService } from '../service/crud-inscription.service'
@Component({
  selector: 'app-update-inscription-form',
  templateUrl: './update-inscription-form.component.html',
  styleUrls: ['./update-inscription-form.component.css']
})
export class UpdateFormationFormComponent implements OnInit {
  UpdateForm: FormGroup;
  getId : any;

  constructor(public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudInscriptionService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneInscription(this.getId).subscribe(res => {
      this.UpdateForm.setValue({
      Nom: res['Nom'],
      Prenom: res['Prenom'],
      Mail: res['Mail'],
      
    });
  });
  this.UpdateForm = this.formBuilder.group({

      Nom: [''],
      Prenom : [''],
      Mail : [''],
          
  })
}

  ngOnInit() {}
    onUpdate(): any {
      this.crudService.updateInscription(this.getId, this.UpdateForm.value)
      .subscribe(() => {
          console.log('Data updated successfully!')
          this.ngZone.run(() => this.router.navigateByUrl('/inscription'))
        }, (err) => {
          console.log(err);
      });
    }
  } 
