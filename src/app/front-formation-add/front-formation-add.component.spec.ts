import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontFormationAddComponent } from './front-formation-add.component';

describe('FrontFormationAddComponent', () => {
  let component: FrontFormationAddComponent;
  let fixture: ComponentFixture<FrontFormationAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontFormationAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontFormationAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
