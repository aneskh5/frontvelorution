import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontInscriptionListComponent } from './front-inscription-list.component';

describe('FrontInscriptionListComponent', () => {
  let component: FrontInscriptionListComponent;
  let fixture: ComponentFixture<FrontInscriptionListComponent>;
  

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontInscriptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontInscriptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
