import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontPublicationShowComponent } from './front-publication-show.component';

describe('FrontPublicationShowComponent', () => {
  let component: FrontPublicationShowComponent;
  let fixture: ComponentFixture<FrontPublicationShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontPublicationShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontPublicationShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
