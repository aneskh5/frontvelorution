import { TestBed } from '@angular/core/testing';

import { CrudCommandeService } from './crud-commande.service';

describe('CrudCommandeService', () => {
  let service: CrudCommandeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudCommandeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
