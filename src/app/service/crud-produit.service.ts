import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
import { ProduitComponent } from '../produit/produit.component';
@Injectable({
  providedIn: 'root'
})
export class CrudProduitService {



  // Node/Express API
  REST_API: string = 'http://localhost:3000/produit/';
  // Http Header
  constructor(private httpClient: HttpClient) { }

  // Add
  Addproduit(data: any, id:any): Observable<any> {
    let API_URL = `${this.REST_API}add`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects
  Getproduits(params:any) {
    return this.httpClient.get<any>('http://localhost:3000/produit/list/'+params.page+'/'+params.limit)

  }

  // Get all objects
  GetCatproduits() {
    return this.httpClient.get<any>('http://localhost:3000/categorie-produit/list')

  }


  GetOneProduitbytitle(titre: any): Observable<any> {
    let API_URL = `${this.REST_API}/get/${titre}`;
    return this.httpClient.get(API_URL).pipe(
      map((res: any) => {
        return res || {};
      }),
    );
  }
  
  GetOneProduit(id: any): Observable<any> {
    let API_URL = `${this.REST_API}/get/${id}`;
    return this.httpClient.get(API_URL).pipe(
      map((res: any) => {
        return res || {};
      }),
    );
  }
  updateProduit(id: any, data: any): Observable<any> {
    let API_URL = `${this.REST_API}/update/${id}`;
    return this.httpClient
      .put(API_URL, data)
      .pipe(catchError(this.handleError));
  }

  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }
  // Delete
  delete(id: any) {
    return this.httpClient.delete('http://localhost:3000/produit/delete/'+id)

  }

}
