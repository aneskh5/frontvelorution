import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class CrudCommandeService {



  // Node/Express API
  REST_API: string = 'http://localhost:3000/commande/';
  // Http Header
  constructor(private httpClient: HttpClient) { }

  // Add
  Addcommande(data: any, id:any): Observable<any> {
    let API_URL = `${this.REST_API}add`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects
  Getcommandes() {
    return this.httpClient.get<any>('http://localhost:3000/commande/list')

  }
  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }
  // Delete
  delete(id: any): Observable<any> {
    let API_URL = `${this.REST_API}delete/${id}`;
    return this.httpClient
      .delete(API_URL)
      .pipe(catchError(this.handleError));

  }
}
