import { TestBed } from '@angular/core/testing';

import { CrudPublicationService } from './crud-publication.service';

describe('CrudPublicationService', () => {
  let service: CrudPublicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudPublicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
