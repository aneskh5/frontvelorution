import { TestBed } from '@angular/core/testing';

import { CrudInscriptionService } from './crud-inscription.service';

describe('CrudFormationService', () => {
  let service: CrudInscriptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudInscriptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
