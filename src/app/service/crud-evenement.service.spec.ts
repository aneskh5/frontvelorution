import { TestBed } from '@angular/core/testing';

import { CrudEvenementService } from './crud-evenement.service';

describe('CrudEvenementService', () => {
  let service: CrudEvenementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudEvenementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
