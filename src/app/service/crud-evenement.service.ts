import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class CrudEvenementService {



  // Node/Express API
  REST_API: string = 'http://localhost:3000/events/';
  // Http Header
  constructor(private httpClient: HttpClient) { }

  // Add
  Addevent(data: any, id:any): Observable<any> {
    let API_URL = `${this.REST_API}add`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects
  Getevents() {
    return this.httpClient.get<any>('http://localhost:3000/events/list')

  }
  myevents() {
    return this.httpClient.get<any>('http://localhost:3000/events/myevent')

  }
  GetOneEvent(id: any): Observable<any> {
      let API_URL = `${this.REST_API}/get/${id}`;
      return this.httpClient.get(API_URL).pipe(
        map((res: any) => {
          return res || {};
        }),
      );
    }
    updateEvent(id: any, data: any): Observable<any> {
      let API_URL = `${this.REST_API}/update/${id}`;
      return this.httpClient
        .put(API_URL, data)
        .pipe(catchError(this.handleError));
    }

  delete(id: any) {
    return this.httpClient.delete('http://localhost:3000/events/delete/'+id)

  }
  checkmeteo(city :any) {
    return this.httpClient.get<any>('http://localhost:3000/events//checkmeteo?city='+city)

  }
  participer(iduser: any, idevent: any): Observable<any> {
    let API_URL = `${this.REST_API}participer/${iduser}/${idevent}`;
    return this.httpClient
      .put(API_URL, iduser,idevent)
      .pipe(catchError(this.handleError));
  }
  cparticiper(iduser: any, idevent: any): Observable<any> {
    let API_URL = `${this.REST_API}cparticiper/${iduser}/${idevent}`;
    return this.httpClient
      .put(API_URL, iduser,idevent)
      .pipe(catchError(this.handleError));
  }
  
  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }
}
