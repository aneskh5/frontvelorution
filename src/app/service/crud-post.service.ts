import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class CrudPostService {



  // Node/Express API
  REST_API: string = 'http://localhost:3000/posts/';
  // Http Header
  constructor(private httpClient: HttpClient) { }

  // Add
  Addpost(data: any, id:any): Observable<any> {
    let API_URL = `${this.REST_API}add/${id}`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects  
  Getposts() {
    return this.httpClient.get<any>('http://localhost:3000/posts/list')

  }
  Geteventposts(id:any) {
    return this.httpClient.get<any>('http://localhost:3000/posts/listpostsevent/'+id)
  }
  GetOnePost(id: any): Observable<any> {
      let API_URL = `${this.REST_API}/get/${id}`;
      return this.httpClient.get(API_URL).pipe(
        map((res: any) => {
          return res || {};
        }),
      );
    }
    updateEvent(id: any, data: any): Observable<any> {
      let API_URL = `${this.REST_API}/update/${id}`;
      return this.httpClient
        .put(API_URL, data)
        .pipe(catchError(this.handleError));
    }

  delete(id: any) {
    return this.httpClient.delete('http://localhost:3000/posts/delete/'+id)

  }
  aime(iduser: any, idpost: any): Observable<any> {
    let API_URL = `${this.REST_API}jaime/${iduser}/${idpost}`;
    return this.httpClient
      .put(API_URL, iduser,idpost)
      .pipe(catchError(this.handleError));
  }
  notaime(iduser: any, idpost: any): Observable<any> {
    let API_URL = `${this.REST_API}jaimeplus/${iduser}/${idpost}`;
    return this.httpClient
      .put(API_URL, iduser,idpost)
      .pipe(catchError(this.handleError));
  }
  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }
}
