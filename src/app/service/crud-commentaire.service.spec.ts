import { TestBed } from '@angular/core/testing';

import { CrudCommentaireService } from './crud-commentaire.service';

describe('CrudCommentaireService', () => {
  let service: CrudCommentaireService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudCommentaireService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
