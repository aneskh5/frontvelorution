import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CrudFormationService {

  // Node/Express API
  REST_API: string = 'http://localhost:3000/formations';
  // Http Header
  constructor(private httpClient: HttpClient) { }

  // Add
  AddFormation(data: any): Observable<any> {
    let API_URL = `${this.REST_API}/add`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects
  GetFormation() {
    return this.httpClient.get<any>('http://localhost:3000/formations/list')
  }

  // Get single object
  GetOneFormation(id: any): Observable<any> {
    let API_URL = `http://localhost:3000/formations/get/`+id;
    return this.httpClient.get(API_URL).pipe(
      map((res: any) => {
        return res || {};
      }),
      catchError(this.handleError)
    );
  }


  // Delete
  deleteFormation(id: any): Observable<any> {
    let API_URL = `${this.REST_API}/delete/`+id;
    return this.httpClient
      .delete(API_URL)
      .pipe(catchError(this.handleError));

  }
  // Update
  updateFormation(id: any, data: any): Observable<any> {
    let API_URL = `http://localhost:3000/formations/update`+id;
    return this.httpClient
      .put(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Gets formation type
  GetFormation_type() {
    return this.httpClient.get<any>('http://localhost:3000/formations/list');
  }

  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }

}
