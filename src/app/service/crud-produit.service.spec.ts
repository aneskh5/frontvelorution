import { TestBed } from '@angular/core/testing';

import { CrudProduitService } from './crud-produit.service';

describe('CrudProduitService', () => {
  let service: CrudProduitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudProduitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
