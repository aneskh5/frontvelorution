import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
import { Inscription_formation } from '../models/Inscription_formations';

@Injectable({
  providedIn: 'root'
})
export class CrudInscriptionService {

  // Node/Express API
  REST_API: string = 'http://localhost:3000/formations-inscription/';
  // Http Header
  constructor(private httpClient: HttpClient) { }

  // Add
  AddInscription(data: any): Observable<any> {
    let API_URL = `http://localhost:3000/formations-inscription/add`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects
  GetInscription() {
    return this.httpClient.get<any>('http://localhost:3000/formations-inscription//list')
  }

  // Get single object
  GetOneInscription(id: any): Observable<any> {
    let API_URL = `http://localhost:3000/formations/get/`+id;
    return this.httpClient.get(API_URL).pipe(
      map((res: any) => {
        return res || {};
      }),
      catchError(this.handleError)
    );
  }
  // // add new formation inscription
  // AddInscriptionParade(data: Inscription_formation, id:any): Observable<any> {
  //   let API_URL = `${this.REST_API}/addInscription/${id}`;
  //   return this.httpClient
  //     .post(API_URL, data)
  //     .pipe(catchError(this.handleError));
  // }

  // Delete
  deleteInscription(id: any): Observable<any> {
    let API_URL = `http://localhost:3000/formations-inscription/delete/`+id;
    return this.httpClient
      .delete(API_URL)
      .pipe(catchError(this.handleError));

  }
  // Update
  updateInscription(id: any, data: any): Observable<any> {
    let API_URL = `http://localhost:3000/formations-inscription/update/`+id;
    return this.httpClient
      .put(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Gets formation type
  // GetFormation_type() {
  //   return this.httpClient.get<any>('http://localhost:3000/formations/list');
  // }

  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }

}
