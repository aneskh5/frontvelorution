
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class CrudCommentaireService {
  totalItems: number = 0;
  currentPage: number   = 0;
  numPages: number = 0;

  params : any = {
    page: 1,
    limit: 25,
  }
  

  // Node/Express API
  REST_API: string = 'http://localhost:3000/commentaire/';
  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private httpClient: HttpClient) { }


  // Add
  AddCommentaire(data: any,id:any): Observable<any> {
  
    let API_URL = `${this.REST_API}add/${id}`;
    console.log(data)
    return this.httpClient.post(API_URL, data).pipe(catchError(this.handleError));
      
  }
  // Get all objects
  // Getpublication(params:any) {
    
  //   return this.httpClient.get<any>('http://localhost:3000/commentaire/list/'+params.page+'/'+params.limit)

  // }
  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }

  GetOneCommentaire(id: any): Observable<any> {
    let API_URL = `${this.REST_API}get/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders }).pipe(
      map((res: any) => {
        return res || {};
      }),
      catchError(this.handleError)
    );
  }
   // Update
  //  updatePublication(id: any, data: any): Observable<any> {
  //   let API_URL = `${this.REST_API}update/${id}`;
  //   return this.httpClient
  //     .put(API_URL, data, { headers: this.httpHeaders })
  //     .pipe(catchError(this.handleError));
  // }
//   deletePublication(id: any): Observable<any> {
//     let API_URL = `${this.REST_API}/delete/${id}`;
//     return this.httpClient
//       .delete(API_URL, { headers: this.httpHeaders })
//       .pipe(catchError(this.handleError));

//   }
//     // Update
//     likePublication(id: any, data: any): Observable<any> {
//       let API_URL = `${this.REST_API}like/${id}`;
//       return this.httpClient
//         .put(API_URL, data, { headers: this.httpHeaders })
//         .pipe(catchError(this.handleError));
// }

GetlistCommentaire(id: any): Observable<any> {
  let API_URL = `${this.REST_API}getByPub/${id}`;
  return this.httpClient.get(API_URL, { headers: this.httpHeaders }).pipe(
    map((res: any) => {
      return res || {};
    }),
    catchError(this.handleError)
  );
}
}