import {Component, NgZone, OnInit} from '@angular/core';
import {CrudCommandeService} from "../service/crud-commande.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {

  commandes:any=[];
  showForm:boolean;

  constructor(private CrudCommandeService: CrudCommandeService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.showForm=false;
      this.CrudCommandeService.Getcommandes().subscribe((res) =>{
        console.log('res data succes ////////////',res)
        this.commandes=res.data
        console.log(this.commandes)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });

      // // ngOnInit() {}
      // onUpdate(): any {
      //   this.crudService.updateProduit(this.getId, this.UpdateForm.value)
      //   .subscribe(() => {
      //       console.log('Data updated successfully!')
      //       this.ngZone.run(() => this.router.navigateByUrl('/produit'))
      //     }, (err) => {
      //       console.log(err);

    }

    show(){
      this.showForm=true;
    }
    

    delete(id:any) {
        {
          this.CrudCommandeService.delete(id).subscribe((data) =>{
              console.log('delete user ==> ',data)
              
            }
          )
        }
      }
    }
  