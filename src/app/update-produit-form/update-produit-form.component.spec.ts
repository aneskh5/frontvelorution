import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProduitFormComponent } from './update-produit-form.component';

describe('UpdateProduitFormComponent', () => {
  let component: UpdateProduitFormComponent;
  let fixture: ComponentFixture<UpdateProduitFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateProduitFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProduitFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
