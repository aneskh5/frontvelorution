import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { CrudProduitService } from '../service/crud-produit.service'
@Component({
  selector: 'app-update-produit-form',
  templateUrl: './update-produit-form.component.html',
  styleUrls: ['./update-produit-form.component.css']
})
export class UpdateProduitFormComponent implements OnInit {
  UpdateForm: FormGroup;
  getId : any;

  constructor(public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudProduitService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneProduit(this.getId).subscribe(res => {
      this.UpdateForm.setValue({
      titre: res['titre'],
      image: res['image'],
      price: res['price'],
      stock: res['stock'],
      
    });
  });
  this.UpdateForm = this.formBuilder.group({

     titre: [''],
     image : [''],
     price : [''],
     stock : [''],
        
  })
}
ngOnInit() {}
onUpdate(): any {
  this.crudService.updateProduit(this.getId, this.UpdateForm.value)
  .subscribe(() => {
      console.log('Data updated successfully!')
      this.ngZone.run(() => this.router.navigateByUrl('/produit'))
    }, (err) => {
      console.log(err);
  });
}

}