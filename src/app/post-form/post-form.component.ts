import { Component, OnInit ,NgZone, Input} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {CrudPostService} from "../service/crud-post.service";
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  postForm: FormGroup;
  selectedType! : any;
  getId : any;

  @Input() idEvent:any ;



  constructor(
              public formBuilder: FormBuilder,
              private router: Router,
              private ngZone: NgZone,
              private CrudPostService: CrudPostService,
              private activatedRoute: ActivatedRoute
              ) {

    this.postForm = this.formBuilder.group({
      content: [''],
      datepost: [''],
    })

  }

  ngOnInit(): void {
    // console.log(typeof (this.id))
    console.log( this.idEvent)


  }

  onSubmit(): any {
    this.CrudPostService.Addpost(this.postForm.value,this.idEvent)
      .subscribe(() => {
        console.log('Data added successfully!')
      }, (err) => {
        console.log(err);
      });
  }
}