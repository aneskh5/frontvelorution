import { Component, OnInit ,NgZone} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {CrudEvenementService} from "../service/crud-evenement.service";
@Component({
  selector: 'app-evenement-form',
  templateUrl: './evenement-form.component.html',
  styleUrls: ['./evenement-form.component.css']
})
export class EvenementFormComponent implements OnInit {


  eventForm: FormGroup;
  selectedType! : any;

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              private ngZone: NgZone,
              private crudService: CrudEvenementService) {

    this.eventForm = this.formBuilder.group({
      titre: [''],
      description: [''],
      city: [''],
      datedebut: [''],
      datefin: [''],
      image: [''],

    })

  }

  ngOnInit(): void {

  }

  onSubmit(): any {
    console.log(this.eventForm.value)
    this.crudService.Addevent(this.eventForm.value, this.selectedType)
      .subscribe(() => {
        console.log('Data added successfully!')
        this.ngZone.run(() => this.router.navigateByUrl('/listeevent'))
      }, (err) => {
        console.log(err);
      });
  }
}