import {Component, NgZone, OnInit} from '@angular/core';
import {CrudProduitService} from "../service/crud-produit.service";
import {Router} from "@angular/router";
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})
export class ProduitComponent implements OnInit {

  produits:any=[];
  showForm:boolean;
  catProduits:any=[];
  backProduits!:any;
  searchbyname: any;
    resData:any;
    numbers:any;
    colTable:any;
    colElement:any ;
    soloExemple!:any;
    showSoloExemple:any;
    searchV:any;
  
    totalExemples: number = 0;
    totalItems: number = 0;
    currentPage: number   = 0;
    numPages: number = 0;
  
    params : any = {
      page: 1,
      limit: 5,
    }
  
    numberItemsFilterList: any = [5,10,25,50,100]
    statuFilterList: any = ["Tous","Envoyer","Approuver","Refuser"]


  constructor(private CrudProduitService: CrudProduitService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.colTable = '12';
      this.getProduits(this.params);
      this.getCatProduits();
      
      // this.showForm=false;
      // this.CrudProduitService.Getproduits().subscribe((res) =>{
      //   console.log('res data succes ////////////',res)
      //   this.produits=res.data
      //   console.log(this.produits)
      // },
      // (err) =>{
      //   console.log('res data err ////////////',err)
      // });
    }

    setPage(pageNo: number): void {
      this.params.page = pageNo; 
      this.getProduits(this.params);
    }

    getProduits(params:any){
 
      this.CrudProduitService.Getproduits(params).subscribe(
  (res) =>{
          this.resData = res.data;
          this.totalItems = res.data.docs.length;
          this.totalExemples = res.data.total;
          this.currentPage = res.data.page;
          this.numPages = res.data.pages;
          this.numbers = Array(res.data.pages+1).fill(0).map((x,i)=>i).slice(1); 
          this.backProduits = this.produits = res.data.docs;
          console.log('res data ',res.data)
        }
        // this.CrudProduitService.Getproduits().subscribe((res) =>{
        //   console.log('res data succes ////////////',res)
        //   this.produits=res.data
        //   console.log(this.produits)
        // },
        // (err) =>{
        //   console.log('res data err ////////////',err)
        // });
      )
    }
    getCatProduits(){
   
      this.CrudProduitService.GetCatproduits().subscribe(
  (res) =>{
          this.catProduits = res.data;
  
        }
      )
    }
    // searchbyname(): void{ 
    //   console.log('hello ',this.searchV.toLowerCase()) 
    //   if(this.searchV =='') 
    //   this.getProduits(this.params); 
    //   this.produits = this.backProduits.filter((item:any)=>{ 
    //     return (item.titre.includes(this.searchV) || item.titre.toLowerCase().includes(this.searchV.toLowerCase())); }); }
    // search(){
    //   const activity = {
    //   search: this.statuFilterList.get('search').value,
    // }
    // this.CrudProduitService.GetOneProduitbytitle(activity.search).subscribe(data => {
    //   this.produits = data.produits;
    // }
    //   )};
    
  
    // showExemple(obj:any){
      
    //   this.soloExemple = obj;
    //   this.colTable = '8';
    //   this.colElement = '4';
    //   this.showSoloExemple = true;
    // }
  
    closeShow(): void{
      this.showSoloExemple = false;
      this.colTable = '12';
      this.colElement = '0';
    }
    
    
    searchStatus(){
      this.getProduits(this.params);
    }
  
    deleteProduit(id:any){
      this.CrudProduitService.delete(id).subscribe(
        (data) =>{
          this.getProduits(this.params);
          console.log('delete produit ==> ',data)
        }
      )
    }
    // addtocart(produit: any){
    //   this.PanierService.addtoCart(produit);
    
  

    show(){
      this.showForm=true;
    }
    

    delete(id:any) {
        {
          this.CrudProduitService.delete(id).subscribe((data) =>{
              console.log('delete user ==> ',data)
              
            }
          )
        }
      }
      reloadCurrentPage() {
        window.location.reload();
       }
    }
  
