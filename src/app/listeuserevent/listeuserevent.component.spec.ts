import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeusereventComponent } from './listeuserevent.component';

describe('ListeusereventComponent', () => {
  let component: ListeusereventComponent;
  let fixture: ComponentFixture<ListeusereventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeusereventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeusereventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
