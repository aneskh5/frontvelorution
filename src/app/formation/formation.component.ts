import {Component, NgZone, OnInit} from '@angular/core';
import {CrudFormationService} from "../service/crud-formation.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.css']
})
export class FormationComponent implements OnInit {


  formations:any=[];
  showForm:boolean;

  constructor(private crudFormationService: CrudFormationService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.showForm=false;
      this.crudFormationService.GetFormation().subscribe((res) =>{
        this.formations= res.data
        console.log(this.formations)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });

    }

    show(){
      this.showForm=true;
    }

    delete(id:any) {
      {
        this.crudFormationService.deleteFormation(id).subscribe(
          (data) =>{
            console.log('delete user ==> ',data)
          }
        )
      }
    }
    reloadCurrentPage() {
      window.location.reload();
     }
      }
    



