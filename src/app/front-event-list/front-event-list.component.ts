import {Component, NgZone, OnInit} from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-front-event-list',
  templateUrl: './front-event-list.component.html',
  styleUrls: ['./front-event-list.component.css']
})
export class FrontEventListComponent implements OnInit {
  events:any=[];

  constructor(private CrudEvenementService: CrudEvenementService,
    private router: Router,
    private ngZone: NgZone,
) { }

  ngOnInit(): void {
    this.CrudEvenementService.Getevents().subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.events=res.data
      console.log(this.events)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });

  }

}
