import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontEventListComponent } from './front-event-list.component';

describe('FrontEventListComponent', () => {
  let component: FrontEventListComponent;
  let fixture: ComponentFixture<FrontEventListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontEventListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontEventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
