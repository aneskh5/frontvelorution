import {Component, NgZone, OnInit} from '@angular/core';
import {CrudPostService} from "../service/crud-post.service";
import {CrudEvenementService} from "../service/crud-evenement.service";

import { Router } from '@angular/router';
import { AuthService } from '../service/users/auth.service';


@Component({
  selector: 'app-dashboard-content',
  templateUrl: './dashboard-content.component.html',
  styleUrls: ['./dashboard-content.component.css']
})
export class DashboardContentComponent implements OnInit {
 nbposts: Number;
 nbevents: any;


  constructor(private CrudPostService: CrudPostService,private CrudEvenementService: CrudEvenementService,private s:AuthService
    ) { }


  ngOnInit(): void {
    this.CrudPostService.Getposts().subscribe((res) =>{
      console.log('res data succes ////////////',res)
      var size = Object.keys(res.data).length;
      this.nbposts=size;
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });
    this.CrudEvenementService.Getevents().subscribe((res) =>{
      console.log('res data succes ////////////',res)
      var size = Object.keys(res.data).length;
      this.nbevents=size;
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });


  }

  logout(){
    this.s.doLogout();
  }
}
