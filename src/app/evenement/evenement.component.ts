import {Component, NgZone, OnInit} from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-evenement',
  templateUrl: './evenement.component.html',
  styleUrls: ['./evenement.component.css']
})
export class EvenementComponent implements OnInit {

  events:any=[];
  showForm:boolean;

  constructor(private CrudEvenementService: CrudEvenementService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.showForm=false;
      this.CrudEvenementService.Getevents().subscribe((res) =>{
        console.log('res data succes ////////////',res)
        this.events=res.data
        console.log(this.events)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });

    }

    show(){
      this.showForm=true;
    }

    delete(id:any) {
      {
        this.CrudEvenementService.delete(id).subscribe(
          (data) =>{
            console.log('delete user ==> ',data)
          }
        )
      }
    }
}
