import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeeventbyuserComponent } from './listeeventbyuser.component';

describe('ListeeventbyuserComponent', () => {
  let component: ListeeventbyuserComponent;
  let fixture: ComponentFixture<ListeeventbyuserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeeventbyuserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeeventbyuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
