import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOneFormationComponent } from './show-one-formation';

describe('FormationComponent', () => {
  let component: ShowOneFormationComponent;
  let fixture: ComponentFixture<ShowOneFormationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowOneFormationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOneFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
