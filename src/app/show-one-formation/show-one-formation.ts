
import { Component, OnInit, NgZone } from '@angular/core';
import {CrudFormationService} from "../service/crud-formation.service";
import {FormBuilder, FormGroup , ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-show-one-formation',
  templateUrl: './show-one-formation.html',
  styleUrls: ['./show-one-formation.css']
})
export class ShowOneFormationComponent implements OnInit {
  UpdateForm: FormGroup;
  getId : any;
  formations:any=[];
  constructor(public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudFormationService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneFormation(this.getId).subscribe(res => {
      
  });
    
   }
   

  
   ngOnInit(): any {
    this.crudService.GetOneFormation(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.formations=res.data
      console.log(this.formations)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });

    
  }




}