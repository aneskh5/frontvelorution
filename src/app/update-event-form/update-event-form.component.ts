import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { CrudEvenementService } from '../service/crud-evenement.service'
@Component({
  selector: 'app-update-event-form',
  templateUrl: './update-event-form.component.html',
  styleUrls: ['./update-event-form.component.css']
})
export class UpdateEventFormComponent implements OnInit {

  UpdateForm: FormGroup;
  getId : any;
  event:any;

   constructor (public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private CrudEvenementService: CrudEvenementService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
       this.CrudEvenementService.GetOneEvent(this.getId).subscribe(res => {
      this.event=res;
      this.UpdateForm.setValue({
        titre:"aaaa",
        description: res['description'],
        city: res['city'],
        datedebut: res['datedebut'],
        datefin: res['datefin'],
    });

      });
  this.UpdateForm = this.formBuilder.group({

      titre:this.event.titre,
      description : [''],
      city : [''],
      datedebut : [''],
      datefin : [''],
  })
}

  async ngOnInit() {
    
      await this.CrudEvenementService.GetOneEvent(this.getId).subscribe(res => {
      this.event=res;

   })
  }
    onUpdate(): any {
      this.CrudEvenementService.updateEvent(this.getId, this.UpdateForm.value)
      .subscribe(() => {
          console.log('Data updated successfully!')
          this.ngZone.run(() => this.router.navigateByUrl('/evenments'))
        }, (err) => {
          console.log(err);
      });
    }
  } 
