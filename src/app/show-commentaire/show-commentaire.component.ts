import { Component, OnInit, NgZone } from '@angular/core';
import {CrudCommentaireService} from "../service/crud-commentaire.service";
import {FormBuilder, FormGroup , ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-commentaire',
  templateUrl: './show-commentaire.component.html',
  styleUrls: ['./show-commentaire.component.css']
})


export class ShowCommentaireComponent implements OnInit {
  UpdateForm: FormGroup;
  getId : any;
  commentaires:any=[];
  like:any;
  constructor(public formBuilder: FormBuilder,
  
    private activatedRoute: ActivatedRoute,
    private crudService: CrudCommentaireService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneCommentaire(this.getId).subscribe(res => {
      
  });
    
   }
   

  
   ngOnInit(): any {
    this.crudService.GetOneCommentaire(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.commentaires=res.data
      this.like =this.commentaires.likes.length;
      console.log(this.commentaires)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });

    
  }




}