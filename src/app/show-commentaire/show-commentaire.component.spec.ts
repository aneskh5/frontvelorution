import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowCommentaireComponent } from './show-commentaire.component';

describe('ShowCommentaireComponent', () => {
  let component: ShowCommentaireComponent;
  let fixture: ComponentFixture<ShowCommentaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowCommentaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowCommentaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
