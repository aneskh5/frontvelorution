import {Component, NgZone, OnInit} from '@angular/core';
import {CrudPublicationService} from "../service/crud-publication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {
  publications:any=[];
  showForm:boolean;
  backExemples!:any;
  resData:any;
  numbers:any;
  colTable:any;
  colElement:any ;
  soloExemple!:any;
  showSoloExemple:any;
  searchV:any;

  totalExemples: number = 0;
  totalItems: number = 0;
  currentPage: number   = 0;
  numPages: number = 0;

  params : any = {
    page: 1,
    limit: 5,
  }
  constructor(private CrudPublicationService: CrudPublicationService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      // this.showForm=false;
      // this.CrudPublicationService.Getpublication().subscribe((res) =>{
      //   console.log('res data succes ////////////',res)
      //   this.publications=res.data
      //   console.log(this.publications)
      // },
      // (err) =>{
      //   console.log('res data err ////////////',err)
      // });
      this.colTable = '12';
    this.getPublication(this.params)

    }
    getPublication(params:any){
 
      this.CrudPublicationService.Getpublication(params).subscribe(
  
        (res) =>{
          this.resData = res.data;
          this.totalItems = res.data.docs.length;
          this.totalExemples = res.data.total;
          this.currentPage = res.data.page;
          this.numPages = res.data.pages;
          this.numbers = Array(res.data.pages+1).fill(0).map((x,i)=>i).slice(1); 
          this.publications = res.data.docs;
          console.log('res data  ////////////',res.data)
        }
      )
    }


    setPage(pageNo: number): void {
      this.params.page = pageNo; 
      this.getPublication(this.params);
    }

    show(){
      this.showForm=true;
    }
    delete(id:any, i:any) {
      console.log(id);
      if(window.confirm('Once deleted, you will no longer be able to retrieve this item. Do you still want to go ahead?')) {
        this.CrudPublicationService.deletePublication(id).subscribe((res) => {
          this.publications.splice(i, 1);

        })
      }
    }

    like(id:any, i:any) {
      console.log(id);
      if(window.confirm('Once deleted, you will no longer be able to retrieve this item. Do you still want to go ahead?')) {
        this.CrudPublicationService.deletePublication(id).subscribe((res) => {
          this.publications.splice(i, 1);

        })
      }
    }

  }