import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontInscriptionAddComponent } from './front-inscription-add.component';

describe('FrontFormationAddComponent', () => {
  let component: FrontInscriptionAddComponent;
  let fixture: ComponentFixture<FrontInscriptionAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontInscriptionAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontInscriptionAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
