import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CrudFormationService} from "../service/crud-formation.service";


@Component({
  selector: 'app-front-inscription-add',
  templateUrl: './front-inscription-add.component.html',
  styleUrls: ['./front-inscription-add.component.css']
})
export class FrontInscriptionAddComponent implements OnInit {
  getId:any;
  formations:any=[];
  constructor(    private activatedRoute: ActivatedRoute,
    
 private crudService: CrudFormationService) { }

  ngOnInit(): void {
    this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    console.log("id == ",this.getId)


    this.crudService.GetOneFormation(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.formations=res.data
      console.log(this.formations)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });
  }

}
