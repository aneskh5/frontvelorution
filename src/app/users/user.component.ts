import {Component, Input, NgZone, OnInit} from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {UserService} from "../service/users/user.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users:any=[];
  showForm:boolean;
  @Input() solo:any; 

  constructor(
    private UserService: UserService,
    private CrudEvenementService: CrudEvenementService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.showForm=false;
      this.UserService.getUsers().subscribe((res) =>{
        console.log('res data succes ////////////',res)
        this.users=res.data
        console.log(this.users)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });

    }

    show(){
      this.showForm=true;
    }

    deleteUser(id:any){
      this.UserService.deleteUser(id).subscribe(
        (data) =>{
          // this.getExemples(this.params);
          console.log('delete user ==> ',data)
        }
      )
    }

}
