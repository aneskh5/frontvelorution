import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/service/users/user.service';
import { AuthService } from '../../service/users/auth.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  currentUser:any;
  error:any;
  res:any;
  message:any;
  showForget:any;
  showCode:any;
  showWritePassword:any;

  constructor(        
    private ar:ActivatedRoute,
    private route:Router,
    private s:UserService
    ) { }

  ngOnInit(): void {
    this.showForget = false;
    this.showCode = false;
    this.showWritePassword = false;
  }
  sendForm(f:any){
    if(!this.showWritePassword){
      console.log('!this.showWritePassword',f);
      this.s.forgetPasword(f).subscribe((res: any) => {
        if(res.response){
          this.currentUser = res.data;
          this.showForget = true;
          this.res = false;
          this.showCode = true;
          
        }else{
          this.res = true;
          this.error = 'error'
          this.message= res.message;
    
        }
      });
    }else{
      console.log('this.showWritePassword',f);

      this.s.sendNewPasword(f).subscribe((res: any) => {
        if(res.response){
          this.route.navigate(['login']);
        }else{
          this.res = true;
          this.error = 'error'
          this.message= res.message;
    
        }
      });
    }

  }
  verifier(f:any){

          // this.showWritePassword = true;
          // this.showCode = false;
          // this.showForget = false;


        this.s.verifierCode(f).subscribe((res: any) => {
        if(res.response){
          this.showWritePassword = true;
          this.showCode = false;
          this.showForget = false;
          this.res = false;
        }else{
          this.res = true;
          this.error = 'error'
          this.message= res.message;
    
        }
      });
  }

}
