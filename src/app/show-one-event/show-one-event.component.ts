import { Component, OnInit} from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-one-event',
  templateUrl: './show-one-event.component.html',
  styleUrls: ['./show-one-event.component.css']
})
export class ShowOneEventComponent implements OnInit {

  getId : any;
  event:any=[];
  constructor(

    private activatedRoute: ActivatedRoute,
    private crudService: CrudEvenementService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
      this.crudService.GetOneEvent(this.getId).subscribe(res => {
      
    });
    
   }
   

  
   ngOnInit(): any {
    this.crudService.GetOneEvent(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.event=res.data
      console.log(this.event)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });



}
checkmeteo(city:any){
  this.crudService.checkmeteo(city ).subscribe((res) =>{
    console.log('res data succes ////////////',res)
    console.log(res)
  },
  (err) =>{
    console.log('res data err ////////////',err)
  });
}
}