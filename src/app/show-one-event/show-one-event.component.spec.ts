import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOneEventComponent } from './show-one-event.component';

describe('ShowOneEventComponent', () => {
  let component: ShowOneEventComponent;
  let fixture: ComponentFixture<ShowOneEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowOneEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOneEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
