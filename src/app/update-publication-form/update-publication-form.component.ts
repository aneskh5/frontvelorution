import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { CrudPublicationService } from '../service/crud-publication.service'

@Component({
  selector: 'app-update-publication-form',
  templateUrl: './update-publication-form.component.html',
  styleUrls: ['./update-publication-form.component.css']
})
export class UpdatePublicationFormComponent implements OnInit {
  UpdateForm: FormGroup;
  getId : any;

  constructor(public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudPublicationService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOnePublication(this.getId).subscribe(res => {
      this.UpdateForm.patchValue({
      Titre: res['Titre'],
      
      Description: res['Description'],
      
    });
  });
  this.UpdateForm = this.formBuilder.group({

     titre: null,
     description :null ,
          
  })
}

  ngOnInit() {}
  
    onUpdate(): any {
      this.crudService.updatePublication(this.getId, this.UpdateForm.value)
      .subscribe(() => {
          console.log('Data updated successfully!')
          this.ngZone.run(() => this.router.navigateByUrl('/publication'))
        }, (err) => {
          console.log(err);
      });
    }

}
