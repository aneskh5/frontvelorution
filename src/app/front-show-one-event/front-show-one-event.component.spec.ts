import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontShowOneEventComponent } from './front-show-one-event.component';

describe('FrontShowOneEventComponent', () => {
  let component: FrontShowOneEventComponent;
  let fixture: ComponentFixture<FrontShowOneEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontShowOneEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontShowOneEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
