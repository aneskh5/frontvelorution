import { Component, OnInit } from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {CrudPostService} from "../service/crud-post.service";

import {  ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-front-show-one-event',
  templateUrl: './front-show-one-event.component.html',
  styleUrls: ['./front-show-one-event.component.css']
})
export class FrontShowOneEventComponent implements OnInit {
  getId : any;
  meteo : any;
  event:any=[];
  data:any;
  icon:any;
  pass :any=[] ;
  idEvent :any ;

  constructor(

    private activatedRoute: ActivatedRoute,
    private crudService: CrudEvenementService,
    private CrudPostService: CrudPostService
    ) 
    
    { 

      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneEvent(this.getId).subscribe(res => {
      
  });
    
   }
   

  
   ngOnInit(): any {
    this.idEvent= this.activatedRoute.snapshot.paramMap.get('id');

    this.crudService.GetOneEvent(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.event=res.data
      this.idEvent=res.data._id
      this.pass.push(res.data._id)
      console.log(this.idEvent)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });



}
checkmeteo(city:any){
  this.crudService.checkmeteo(city ).subscribe((res) =>{
    console.log('res data succes ////////////',res)
    console.log(res.description)
    this.meteo=res.description
    this.icon=res.icon

  },
  (err) =>{
    console.log('res data err ////////////',err)
  });
}
participer(idevent:any,iduser:any){
  this.crudService.participer(idevent,iduser).subscribe((res) =>{
    console.log('res data succes ////////////',res)
    console.log(res)

  },
  (err) =>{
    console.log('res data err ////////////',err)
  });
}
cparticiper(idevent:any,iduser:any){
  this.crudService.cparticiper(idevent,iduser).subscribe((res) =>{
    console.log('res data succes ////////////',res)
    console.log(res)

  },
  (err) =>{
    console.log('res data err ////////////',err)
  });
  
}
posts(idevent:any){
  this.CrudPostService.Geteventposts(idevent).subscribe((res) =>{
    console.log('res data succes ////////////',res)
    console.log(res)

  },
  (err) =>{
    console.log('res data err ////////////',err)
  });
}


}