import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularPaginatorModule } from 'angular-paginator';

import {MenuComponent} from "./menu/menu.component";
import {DashboardContentComponent} from "./dashboard-content/dashboard-content.component";
import {NavbarAdminComponent} from "./navbar-admin/navbar-admin.component";
import {LoginComponent} from "./login/login.component";
import {HomepageContentComponent} from "./homepage-content/homepage-content.component";
import {FormationComponent} from "./formation/formation.component";
import {UpdateFormationFormComponent} from './update-formation-form/update-formation-form.component';
import { InscriptionFormationFormComponent } from './inscription-formation-form/inscription-formation-form.component';
import { ClientFormationComponent } from './client-formation/client-formation.component';
import {EvenementComponent} from "./evenement/evenement.component";
import {FrontEventListComponent} from "./front-event-list/front-event-list.component";
import {FrontEventAddComponent} from "./front-event-add/front-event-add.component";
import {ProduitComponent} from "./produit/produit.component";
// import {PublicationComponent} from "./publication/publication.component";
import {FrontFormationListComponent} from "./front-formation-list/front-formation-list.component";
import {FrontFormationAddComponent} from "./front-formation-add/front-formation-add.component";
import {FrontInscriptionAddComponent} from "./front-inscription-add/front-inscription-add.component";
import {FrontInscriptionListComponent} from "./front-inscription-list/front-inscription-list.component";
import {InscriptionComponent} from "./inscription/inscription.component";
import { ShowOneFormationComponent } from './show-one-formation/show-one-formation';
import {ShowFormComponent} from './show-form/show-form.component';
// import {UpdateInscriptionFormComponent} from './update-inscription-form/update-inscription-form.component';





import {UpdateEventFormComponent} from "./update-event-form/update-event-form.component";
import {PostComponent} from "./post/post.component";
import {PostFormComponent} from "./post-form/post-form.component";
import {ShowOneEventComponent} from "./show-one-event/show-one-event.component";
import {FrontShowOneEventComponent} from "./front-show-one-event/front-show-one-event.component";
import {FrontShowEventPostsComponent} from "./front-show-event-posts/front-show-event-posts.component";
import {FrontPostAddComponent} from "./front-post-add/front-post-add.component";
import {MyeventsComponent} from "./myevents/myevents.component";

import {UserComponent} from "./users/user.component";
import {EditUserComponent} from "./users-edit/edit-user.component";
import {ForgetPasswordComponent} from "./users/forget-password/forget-password.component";
import { AuthGuard } from "./shared/auth.guard";
import {FormationFormComponent} from "./formation-form/formation-form.component";
import {PublicationComponent} from "./publication/publication.component";
import {UpdatePublicationFormComponent} from './update-publication-form/update-publication-form.component';
import {ShowOnePubComponent} from './show-one-pub/show-one-pub.component';
import {FrontPublicationAddComponent} from './front-publication-add/front-publication-add.component';
import {FrontPublicationListComponent} from "./front-publication-list/front-publication-list.component";
import {FrontPublicationShowComponent} from './front-publication-show/front-publication-show.component';






import { UpdateProduitFormComponent } from './update-produit-form/update-produit-form.component';
import {FrontProduitAddComponent} from "./front-produit-add/front-produit-add.component";
import {FrontProduitListComponent} from "./front-produit-list/front-produit-list.component";
//import { FrontProduitShowComponent } from './front-produit-show/front-produit-show.component';
import { CommandeComponent } from './commande/commande.component';
import { NavbarProduitComponent } from './navbar-produit/navbar-produit.component';
import { PanierComponent } from './panier/panier.component';





const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {path:'Navbar-admin', component: NavbarAdminComponent},
  {path:'Navbar-admin', component: NavbarAdminComponent},
  {path:'dashboard', component: DashboardContentComponent,canActivate: [AuthGuard]},
  {path:'login', component: LoginComponent},
  {path:'home', component: HomepageContentComponent},
  {path:'formation', component: FormationComponent},
  {path:'listFormations', component: ClientFormationComponent},
  {path:'InscriptionFormation', component: InscriptionFormationFormComponent},
  {path:'edit-formation/:id', component: UpdateFormationFormComponent},
  {path:'update-event/:id', component: UpdateEventFormComponent},

  {path:'publication/edit-publication/:id', component: UpdatePublicationFormComponent},
  {path:'evenments', component: EvenementComponent},
  {path:'listeevent', component:FrontEventListComponent },
  {path:'addevent', component:FrontEventAddComponent },
  {path:'produit', component:ProduitComponent },
  {path:'publication', component:PublicationComponent },


  {path:'posts', component:PostComponent },
  {path:'oneevent/:id', component:ShowOneEventComponent },
  {path:'oneeventfront/:id', component:FrontShowOneEventComponent },
  {path:'eventposts/:id', component:FrontShowEventPostsComponent },
  {path:'addpost/:id', component:FrontPostAddComponent },
  {path:'addpostform/:id', component:PostFormComponent },
  {path:'myevents', component:MyeventsComponent },

  {path:'users', component:UserComponent },
  {path:'edit-user/:id', component: EditUserComponent},
  {path:'forget-password', component: ForgetPasswordComponent},

  




  {path:'listformation', component:FrontFormationListComponent },
  {path:'addformation', component:FrontFormationAddComponent },
  {path:'addinscription/:id', component:FrontInscriptionAddComponent },
  {path:'listinscription', component:FrontInscriptionListComponent },
  {path:'inscription', component: InscriptionComponent},
  {path:'showone/:id', component: ShowOneFormationComponent},
  {path:'show', component: ShowFormComponent},
  {path:'edit-formation/:id', component: UpdateFormationFormComponent},
  // {path:'edit-inscription/:id', component: UpdateInscriptionFormComponent}


  {path:'publication', component:PublicationComponent },
  {path:'publication/ShowOnePub/:id', component: ShowOnePubComponent},
  {path:'addpublication', component:FrontPublicationAddComponent },
  {path:'listepublication', component:FrontPublicationListComponent },
  {path:'listepublication/OneShow/:id', component:FrontPublicationShowComponent },
  {path:'update-produit/:id', component: UpdateProduitFormComponent},
  {path:'addproduit', component:FrontProduitAddComponent },
  {path:'listproduit', component:FrontProduitListComponent },
  //{path:'showproduit', component:FrontProduitShowComponent},
  {path:'commande', component:CommandeComponent },
  {path:'panier', component:PanierComponent}










];

@NgModule({
  imports: [RouterModule.forRoot(routes),AngularPaginatorModule],
  exports: [RouterModule]
  
})
export class AppRoutingModule { }
