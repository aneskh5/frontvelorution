import {Component, NgZone, OnInit} from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-myevents',
  templateUrl: './myevents.component.html',
  styleUrls: ['./myevents.component.css']
})
export class MyeventsComponent implements OnInit {
  events:any=[];
  id:any="62556325r"

  constructor(private CrudEvenementService: CrudEvenementService,
    private router: Router,
    private ngZone: NgZone,

  ) { }

  ngOnInit(): void {
    
    this.CrudEvenementService.myevents().subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.events=res.data
      console.log(this.events)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });

  }
    
  }


