import { Component, OnInit ,NgZone} from '@angular/core';
import {FormBuilder, FormGroup , ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Router} from "@angular/router";
import {CrudPublicationService} from "../service/crud-publication.service";
import { Cloudinary } from '@cloudinary/angular-5.x';
import { FileUploader, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import { AngularPaginatorModule } from 'angular-paginator';

@Component({
  selector: 'app-publication-form',
  templateUrl: './publication-form.component.html',
  styleUrls: ['./publication-form.component.css']
})
export class PublicationFormComponent implements OnInit {

  publicationForm: FormGroup;
  selectedType! : any;
  showForm:boolean;
  constructor(public formBuilder: FormBuilder,public ReactiveFormsModule:ReactiveFormsModule,
              private router: Router,
              private ngZone: NgZone,
              private crudService: CrudPublicationService,
              private ReactiveForms: ReactiveFormsModule,) {

    this.publicationForm = this.formBuilder.group({
      titre: [''],
      description: [''],
      image: [''],
      
    })

  }

  ngOnInit(): void {

  }
  show(){
    this.showForm=true;
  }

  onSubmit(): any {
    console.log(this.publicationForm.value)
    this.crudService.Addpublication(this.publicationForm.value, this.selectedType)
  
      .subscribe(() => {
        console.log('Data added successfully!')
        this.ngZone.run(() => this.router.navigateByUrl('/publication'))
      }, (err) => {
        console.log(err);
      });
      this.showForm=false;
  }
  

  
}