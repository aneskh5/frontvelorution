import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontProduitListComponent } from './front-produit-list.component';

describe('FrontProduitListComponent', () => {
  let component: FrontProduitListComponent;
  let fixture: ComponentFixture<FrontProduitListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontProduitListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontProduitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
