import { Component, OnInit} from '@angular/core';
import {CrudFormationService} from "../service/crud-formation.service";
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-form',
  templateUrl: './show-form.component.html',
  styleUrls: ['./show-form.component.css']
})
export class ShowFormComponent implements OnInit {

  getId : any;
  formations:any=[];
  constructor(

    private activatedRoute: ActivatedRoute,
    private crudService: CrudFormationService) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
      this.crudService.GetOneFormation(this.getId).subscribe(res => {
      
    });
    
   }
   

  
   ngOnInit(): any {
    this.crudService.GetOneFormation(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.formations=res.data
      console.log(this.formations)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });



}
// checkmeteo(city:any){
//   this.crudService.checkmeteo(city ).subscribe((res) =>{
//     console.log('res data succes ////////////',res)
//     console.log(res)
//   },
//   (err) =>{
//     console.log('res data err ////////////',err)
//   });
// }
}