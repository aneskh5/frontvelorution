import { Component, OnInit ,NgZone} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {CrudCommandeService} from "../service/crud-commande.service";
@Component({
  selector: 'app-commande-form',
  templateUrl: './commande-form.component.html',
  styleUrls: ['./commande-form.component.css']
})
export class CommandeFormComponent implements OnInit {

  commandeForm: FormGroup;
  selectedType! : any;

  constructor(public formBuilder: FormBuilder,
              private router: Router,
              private ngZone: NgZone,
              private crudService: CrudCommandeService) {

    this.commandeForm = this.formBuilder.group({
      titre: [''],
      image: [''],
      price: [''],
      stock: [''],
      
    })

  }

  ngOnInit(): void {

  }

  onSubmit(): any {
    this.crudService.Addcommande(this.commandeForm.value, this.selectedType)
      .subscribe(() => {
        console.log('Data added successfully!')
        this.ngZone.run(() => this.router.navigateByUrl('/commandes'))
      }, (err) => {
        console.log(err);
      });
  }
}