import { Component, OnInit, NgZone, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudInscriptionService } from '../service/crud-inscription.service';
import { Inscription_formation } from '../models/Inscription_formations';
import {CrudFormationService} from "../service/crud-formation.service";

@Component({
  selector: 'app-inscription-formation-form',
  templateUrl: './inscription-formation-form.component.html',
  styleUrls: ['./inscription-formation-form.component.css']
})
export class InscriptionFormationFormComponent implements OnInit {
  id:any;
  InscriptionForm: FormGroup;
  formations:any=[];
  selectedType! : any;
  getId:any;
  titre :any;
  constructor(public formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private ngZone: NgZone,
    private crudService: CrudInscriptionService,private crudServic: CrudFormationService) {

      this.InscriptionForm = this.formBuilder.group({
        Nom: [''],
        Prenom : [''],
        Mail : [''],
        id_formation : [''],
        TitreDeFormation: ['']
      })

     }

     ngOnInit(): void {
      console.log("ngOnInit  ==== ",      )
      this.id = this.activatedRoute.snapshot.paramMap.get('id') ;
      this.crudService.GetInscription().subscribe(res => {
        console.log(res)
        this.formations =res;
      }); 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
      this.crudServic.GetOneFormation(this.getId).subscribe(res => {
        
    });
    // console.log('test,this.formations.TitreDeFormation')
//   var form;
//  let formationsss:any=[];
      this.crudServic.GetOneFormation(this.getId ).subscribe((res) =>{
        console.log('res data succes ////////////',res)
        this.formations=res.data
     
        console.log(this.formations)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });
      // formationsss= this.formations
      // console.log('test"',this.titre)
      this.InscriptionForm = this.formBuilder.group({
        Nom: [''],
        Prenom : [''],
        Mail : [''],
        id_formation : ['']
      })
    }
  onSubmit(): any {
    let form = this.InscriptionForm.value;
    form.id_formation = this.id;
    console.log("form  ==== ",form)
    this.crudService.AddInscription(this.InscriptionForm.value)
    .subscribe(() => {
        console.log('Data added successfully!')
        this.router.navigate(['listformation']);

      }, (err) => {
        console.log(err);
    });
  }

}
