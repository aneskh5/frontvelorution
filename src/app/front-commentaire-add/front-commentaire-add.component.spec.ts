import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontCommentaireAddComponent } from './front-commentaire-add.component';

describe('FrontCommentaireAddComponent', () => {
  let component: FrontCommentaireAddComponent;
  let fixture: ComponentFixture<FrontCommentaireAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontCommentaireAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontCommentaireAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
