import { Component, OnInit } from '@angular/core';
import {CrudEvenementService} from "../service/crud-evenement.service";
import {CrudPostService} from "../service/crud-post.service";

import {  ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-front-show-event-posts',
  templateUrl: './front-show-event-posts.component.html',
  styleUrls: ['./front-show-event-posts.component.css']
})
export class FrontShowEventPostsComponent implements OnInit {
  getId : any;
  meteo : any;
  posts:any=[];
  jaime:any;
  jaimeplus:any;

  constructor    (private activatedRoute: ActivatedRoute,
  private crudService: CrudEvenementService,
  private CrudPostService: CrudPostService
  ) 
  
  { 
    this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneEvent(this.getId).subscribe(res => {
    
});
}

  ngOnInit(): void {
    this.CrudPostService.Geteventposts(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.posts=res.data
      console.log(this.posts)
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });

    
  }
  aime(idevent:any,iduser:any){
    this.CrudPostService.aime(idevent,iduser).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      console.log(res)
  
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });
  }
  notaime(idevent:any,iduser:any){
    this.CrudPostService.notaime(idevent,iduser).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      console.log(res.data)
      window.alert(res.data)
  
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });
}

}
