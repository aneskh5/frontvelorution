import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontShowEventPostsComponent } from './front-show-event-posts.component';

describe('FrontShowEventPostsComponent', () => {
  let component: FrontShowEventPostsComponent;
  let fixture: ComponentFixture<FrontShowEventPostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontShowEventPostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontShowEventPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
