import {Component, NgZone, OnInit, ChangeDetectorRef} from '@angular/core';
import {CrudPublicationService} from "../service/crud-publication.service";
import {Router} from "@angular/router";
import { AngularPaginatorModule } from 'angular-paginator';


@Component({
  selector: 'app-front-publication-list',
  templateUrl: './front-publication-list.component.html',
  styleUrls: ['./front-publication-list.component.css']
})

export class FrontPublicationListComponent implements OnInit {
   publications:any=[];
   
   backExemples!:any;
   resData:any;
   numbers:any;
   colTable:any;
   colElement:any ;
   soloExemple!:any;
   showSoloExemple:any;
   searchV:any;
 
   totalExemples: number = 0;
   totalItems: number = 0;
   currentPage: number   = 0;
   numPages: number = 0;
 
   params : any = {
     page: 1,
     limit: 5,
   }

   statuFilterList: any = ["Tous","Envoyer","Approuver","Refuser"]
  constructor(private CrudPublicationService: CrudPublicationService,
    private router: Router,
    private ngZone: NgZone,
    private cdRef : ChangeDetectorRef
   
    
) { }
setPage(pageNo: number): void {
  this.params.page = pageNo; 
  this.getPublication(this.params);
}

  ngOnInit(): void {
    this.colTable = '12';
    this.getPublication(this.params)
    }
    

  
  getPublication(params:any){
 
    this.CrudPublicationService.Getpublication(params).subscribe(

      (res) =>{
        this.resData = res.data;
        this.totalItems = res.data.docs.length;
        this.totalExemples = res.data.total;
        this.currentPage = res.data.page;
        this.numPages = res.data.pages;
        this.numbers = Array(res.data.pages+1).fill(0).map((x,i)=>i).slice(1); 
        this.publications = res.data.docs;
        console.log('res data  ////////////',res.data)
      }
    )
  }

  
}
