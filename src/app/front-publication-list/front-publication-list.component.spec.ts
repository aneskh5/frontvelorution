import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontPublicationListComponent } from './front-publication-list.component';

describe('FrontPublicationListComponent', () => {
  let component: FrontPublicationListComponent;
  let fixture: ComponentFixture<FrontPublicationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontPublicationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontPublicationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
