import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';
import { NavbarClientComponent } from './navbar-client/navbar-client.component';
import { NavbarAdminComponent } from './navbar-admin/navbar-admin.component';
import { HomepageContentComponent } from './homepage-content/homepage-content.component';
import { HomepageFooterComponent } from './homepage-footer/homepage-footer.component';
import { LoginComponent } from './login/login.component';
import { FormationFormComponent } from './formation-form/formation-form.component';
import { FormationComponent } from './formation/formation.component';
import { UpdateFormationFormComponent } from './update-formation-form/update-formation-form.component';
import { InscriptionFormationFormComponent } from './inscription-formation-form/inscription-formation-form.component';
import { ClientFormationComponent } from './client-formation/client-formation.component';
import { EmailFormationFormComponent } from './email-formation-form/email-formation-form.component';
import { MessageService } from './service/message.service';
import { EvenementComponent } from './evenement/evenement.component';
import { EvenementFormComponent } from './evenement-form/evenement-form.component';
import { FrontEventListComponent } from './front-event-list/front-event-list.component';
import { FrontEventAddComponent } from './front-event-add/front-event-add.component';
import { ProduitComponent } from './produit/produit.component';
import { ProduitFormComponent } from './produit-form/produit-form.component';
import { PublicationComponent } from './publication/publication.component';
import { PublicationFormComponent } from './publication-form/publication-form.component';
import {FrontFormationListComponent} from "./front-formation-list/front-formation-list.component";
import {FrontFormationAddComponent} from "./front-formation-add/front-formation-add.component";
import {FrontInscriptionAddComponent} from "./front-inscription-add/front-inscription-add.component";
import {FrontInscriptionListComponent} from "./front-inscription-list/front-inscription-list.component";
import { InscriptionComponent } from './inscription/inscription.component';
import { ShowOneFormationComponent } from './show-one-formation/show-one-formation';
import { ShowFormComponent } from './show-form/show-form.component';
// import {UpdateInscriptionFormComponent} from './update-inscription-form/update-inscription-form.component';





import { UpdateEventFormComponent } from './update-event-form/update-event-form.component';
import { PostComponent } from './post/post.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ShowOneEventComponent } from './show-one-event/show-one-event.component';
import { FrontShowOneEventComponent } from './front-show-one-event/front-show-one-event.component';
import { MeteoComponent } from './meteo/meteo.component';
import { ListeeventbyuserComponent } from './listeeventbyuser/listeeventbyuser.component';
import { ListeusereventComponent } from './listeuserevent/listeuserevent.component';
import { FrontShowEventPostsComponent } from './front-show-event-posts/front-show-event-posts.component';
import { FrontPostAddComponent } from './front-post-add/front-post-add.component';
import { MyeventsComponent } from './myevents/myevents.component';

import { UserComponent } from './users/user.component';
import { EditUserComponent } from './users-edit/edit-user.component';
import {ForgetPasswordComponent} from "./users/forget-password/forget-password.component";
import { UpdatePublicationFormComponent } from './update-publication-form/update-publication-form.component';
import { ShowOnePubComponent } from './show-one-pub/show-one-pub.component';
import { FrontPublicationAddComponent } from './front-publication-add/front-publication-add.component';
import { FrontPublicationListComponent } from './front-publication-list/front-publication-list.component';
import { FrontPublicationShowComponent } from './front-publication-show/front-publication-show.component';
import { ShowCommentaireComponent } from './show-commentaire/show-commentaire.component';
import { FrontCommentaireAddComponent } from './front-commentaire-add/front-commentaire-add.component';
import { CommentaireFormComponent } from './commentaire-form/commentaire-form.component';



import { UpdateProduitFormComponent } from './update-produit-form/update-produit-form.component';
import { FrontProduitAddComponent } from './front-produit-add/front-produit-add.component';
import {FrontProduitListComponent} from "./front-produit-list/front-produit-list.component";
import { CommandeComponent } from './commande/commande.component'; 
import { CommandeFormComponent } from './commande-form/commande-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { NavbarProduitComponent } from './navbar-produit/navbar-produit.component';
import { NavbarClientProduitComponent } from './navbar-client-produit/navbar-client-produit.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PanierComponent } from './panier/panier.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardContentComponent,
    NavbarClientComponent,
    NavbarAdminComponent,
    HomepageContentComponent,
    HomepageFooterComponent,
    LoginComponent,
    FormationFormComponent,
    FormationComponent,
    UpdateFormationFormComponent,
    InscriptionFormationFormComponent,
    ClientFormationComponent,
    EmailFormationFormComponent,
    EvenementComponent,
    EvenementFormComponent,
    FrontEventListComponent,
    FrontEventAddComponent,
    ProduitComponent,
    ProduitFormComponent,
    PublicationComponent,
    PublicationFormComponent,

    UpdateEventFormComponent,
    PostComponent,
    PostFormComponent,
    ShowOneEventComponent,
    FrontShowOneEventComponent,
    MeteoComponent,
    ListeeventbyuserComponent,
    ListeusereventComponent,
    FrontShowEventPostsComponent,
    FrontPostAddComponent,
    MyeventsComponent,


    UserComponent,
    EditUserComponent,
    ForgetPasswordComponent,

    FrontFormationAddComponent,
    FrontFormationListComponent,
    FrontInscriptionAddComponent,
    FrontInscriptionListComponent,
    InscriptionComponent,
    ShowOneFormationComponent,
    ShowFormComponent,
    // UpdateInscriptionFormComponent
    UpdatePublicationFormComponent,
    ShowOnePubComponent,
    FrontPublicationAddComponent,
    FrontPublicationListComponent,
    FrontPublicationShowComponent,
    ShowCommentaireComponent,
    FrontCommentaireAddComponent,
    CommentaireFormComponent,
    ProduitComponent,
    UpdateProduitFormComponent,
    FrontProduitAddComponent,
    FrontProduitListComponent,
    CommandeComponent,
    CommandeFormComponent,
    NavbarProduitComponent,
    NavbarClientProduitComponent,
    PanierComponent,
   
  ],
   imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    Ng2SearchPipeModule,

    
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
