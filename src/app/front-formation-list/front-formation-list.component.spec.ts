import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontFormationListComponent } from './front-formation-list.component';


describe('FrontFormationListComponent', () => {
  let component: FrontFormationListComponent;
  let fixture: ComponentFixture<FrontFormationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontFormationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontFormationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
