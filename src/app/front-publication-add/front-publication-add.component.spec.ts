import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontPublicationAddComponent } from './front-publication-add.component';

describe('FrontPublicationAddComponent', () => {
  let component: FrontPublicationAddComponent;
  let fixture: ComponentFixture<FrontPublicationAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontPublicationAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontPublicationAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
