import { Component, OnInit } from '@angular/core';
import { PanierService } from '../service/panier.service';


@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  public products : any = [];
  public grandTotal !: number;
  constructor(private PanierService : PanierService) { }

  ngOnInit(): void {
    this.PanierService.getProducts()
    .subscribe(res=>{
      this.products = res;
      this.grandTotal = this.PanierService.getTotalPrice();
    })
  }
  removeItem(item: any){
    this.PanierService.removeCartItem(item);
  }
  emptycart(){
    this.PanierService.removeAllCart();
  }

}
