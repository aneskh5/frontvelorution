import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontPostAddComponent } from './front-post-add.component';

describe('FrontPostAddComponent', () => {
  let component: FrontPostAddComponent;
  let fixture: ComponentFixture<FrontPostAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontPostAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontPostAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
