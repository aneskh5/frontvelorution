
import { Component, OnInit, NgZone } from '@angular/core';
import {CrudPublicationService} from "../service/crud-publication.service";
import {CrudCommentaireService} from "../service/crud-commentaire.service";
import {FormBuilder, FormGroup , ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-show-one-pub',
  templateUrl: './show-one-pub.component.html',
  styleUrls: ['./show-one-pub.component.css']
})
export class ShowOnePubComponent implements OnInit {
  UpdateForm: FormGroup;
  getId : any;
  publications:any=[];
  commentaires:any=[];
  like:any;
  message : any;
  constructor(public formBuilder: FormBuilder,
  
    private activatedRoute: ActivatedRoute,
    private crudService: CrudPublicationService,
    private crudServicecom: CrudCommentaireService
    ) 
    
    { 
      this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOnePublication(this.getId).subscribe(res => {
      
  });
    
   }
   

  
   ngOnInit(): any {
    this.message=this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOnePublication(this.getId ).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.publications=res.data
      this.like =this.publications.likes.length;
      console.log(this.publications)//GetlistCommentaire
      
    },(err) =>{
      console.log('res data err ////////////',err)
    });
    this.crudServicecom.GetlistCommentaire(this.getId).subscribe((res) =>{
      console.log('res data succes ////////////',res)
      this.commentaires=res.data
      
      console.log(this.commentaires)//GetlistCommentaire
      
    },
    (err) =>{
      console.log('res data err ////////////',err)
    });

    
  }




}