import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOnePubComponent } from './show-one-pub.component';

describe('ShowOnePubComponent', () => {
  let component: ShowOnePubComponent;
  let fixture: ComponentFixture<ShowOnePubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowOnePubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOnePubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
