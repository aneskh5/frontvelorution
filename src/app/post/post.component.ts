import {Component, NgZone, OnInit} from '@angular/core';
import {CrudPostService} from "../service/crud-post.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts:any=[];
  showForm:boolean;

  constructor(private CrudPostService: CrudPostService,
    private router: Router,
    private ngZone: NgZone,

    ) { }

    ngOnInit(): void {
      this.showForm=false;
      this.CrudPostService.Getposts().subscribe((res) =>{
        console.log('res data succes ////////////',res)
        this.posts=res.data
        console.log(this.posts)
      },
      (err) =>{
        console.log('res data err ////////////',err)
      });

    }

    show(){
      this.showForm=true;
    }

    delete(id:any) {
      {
        this.CrudPostService.delete(id).subscribe(
          (data) =>{
            console.log('delete post ==> ',data)
          }
        )
      }
    }
}
