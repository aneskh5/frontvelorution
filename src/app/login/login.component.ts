import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../service/users/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  currentUser:any;
  error:any;
  res:any;
  message:any;

  constructor(        
    private ar:ActivatedRoute,
    private route:Router,
    private s:AuthService
    ) { }

  ngOnInit(): void {
  }
  signIn(f:any){
    console.log('login form',f);
    this.s.signIn(f).subscribe((res: any) => {
      if(res.response){
        localStorage.setItem('access_token', res.token);
        this.currentUser = res.data;
        this.res = false;
        if(this.currentUser.role == "admin"){
          this.route.navigate(['dashboard']);
        }else{
          this.route.navigate(['home']);
        }
      }else{
        this.res = true;
        this.error = 'error'
        this.message= res.message;
  
      }
    });
  }
}
